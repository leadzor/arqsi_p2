﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using IDEIBiblio.Models;

namespace IDEIBiblioCatalogService
{
    public class CatalogService : ICatalogService
    {

        IDEIBiblioContext db = new IDEIBiblioContext();

        public List<CatalogPayload> GetCatalog()
        {
            List<Item> dblist = db.Items.ToList();
            List<CatalogPayload> payload = new List<CatalogPayload>();
            foreach (Item i in dblist)
            {
                payload.Add(new CatalogPayload() { GlobalID = i.GUID, InternalID = i.ItemID, Price = i.Preco });
            }
            return payload;           
        }

        public string SendList(string WSAuthKey, List<CatalogPayload> inputList)
        {
            try
            {
                IDEIBiblioContext db = new IDEIBiblioContext();
                // se um marketer identificado
                if (db.Marketers.Any(m => m.AuthKey == WSAuthKey))
                {
                    // criar uma tabela de precos
                    Marketer m = db.Marketers.Single(mk => mk.AuthKey == WSAuthKey);
                    TabelaPrecos tabela = new TabelaPrecos();
                    tabela.DataHora = DateTime.Now;
                    tabela.Marketer = m;

                    db.TabelaPrecos.Add(tabela);

                    // por cada linha do pacote de input, criar 1 linha na base de dados
                    string output = inputList.Count()+"->>>";
                    foreach (CatalogPayload cp in inputList)
                    {
                        LinhaTabelaPrecos tlp = new LinhaTabelaPrecos();
                        if (db.Items.Any(i => i.ItemID == cp.InternalID))
                        {
                            Item item = db.Items.Find(cp.InternalID);
                            // verificacao de preco ser positivo
                            if(cp.Price > 0)
                            {
                                output += "[";
                                LinhaTabelaPrecos ltp = new LinhaTabelaPrecos()
                                    {
                                        TabelaPrecos = tabela,
                                        Item = item,
                                        NovoPreco = cp.Price
                                    };
                                output += cp.GlobalID.ToString()+ "|" + cp.InternalID.ToString() + "|" + cp.Price.ToString();
                                db.LinhasTPrecos.Add(ltp);
                                output += "]  ";
                            }
                        }
                    }
                    db.SaveChanges();
                    return output;
                }
                db.Erros.Add(new Erro() { Descricao = "Access Denied - " + this.GetType() + " - " + WSAuthKey, When = DateTime.Now, Gravidade = 4 });
                db.SaveChanges();
                return "DENIED";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

    }
}
