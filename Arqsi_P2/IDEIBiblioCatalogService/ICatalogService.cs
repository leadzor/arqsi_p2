﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IDEIBiblioCatalogService
{
    [ServiceContract]
    public interface ICatalogService
    {
        [OperationContract] List<CatalogPayload> GetCatalog();
        [OperationContract] string SendList(string WSAuthKey, List<CatalogPayload> inputList);
    }


    [DataContract]
    public class CatalogPayload
    {
        [DataMember] public int InternalID { get; set; }
        [DataMember] public string GlobalID { get; set; }
        [DataMember] public decimal Price { get; set; }
    }
    
}
