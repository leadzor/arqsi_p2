﻿using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Filters;

namespace IDEIBiblio
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new InitializeSimpleMembershipAttribute());
        }
    }
}