﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Carrinho
    {

        public int CarrinhoID { get; set; }
        public string AnonCarrinhoID { get; set; }
        public int ItemID { get; set; }
        public int Numero { get; set; }
        public DateTime DataC { get; set; }

        public virtual Item Item { get; set; }
    }
}