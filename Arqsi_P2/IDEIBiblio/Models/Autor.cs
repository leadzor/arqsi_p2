﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Autor
    {
        public int AutorID { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Biografia { get; set; }

        // Propriedades navegacao
        public virtual ICollection<Item> Items { get; set; }
    }
}