﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Genero
    {
        // Chave primaria
        public int GeneroID { get; set; }

        // Propriedades
        [DisplayName("Género")]
        public string Nome { get; set; }

        // Propriedades navegacao
        public virtual ICollection<Item> Items { get; set; }
    }
}