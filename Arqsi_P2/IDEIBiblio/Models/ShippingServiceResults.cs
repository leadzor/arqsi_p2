﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDEIBiblio.Models
{
    public class ShippingServiceResults
    {
        public decimal Price { get; set; }
        public string ShippingService { get; set; }
    }
}
