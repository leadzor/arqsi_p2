﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public interface IExternalShipping
    {
        decimal GetPrecoPorPagina(int numPaginas);
        string GetNomeServico();
    }
}