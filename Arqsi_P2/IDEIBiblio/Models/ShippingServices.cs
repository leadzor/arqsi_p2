﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class ShippingAll : IExternalShipping
    {

        private string Nome = "ShippingAll";
        private ShippingAllWS.ShippingAllPortTypeClient proxy;

        public ShippingAll()
        {
             proxy = new ShippingAllWS.ShippingAllPortTypeClient();
        }

        public decimal GetPrecoPorPagina(int numPaginas)
        {
            return proxy.prpag(numPaginas);
        }


        public string GetNomeServico()
        {
            return Nome;
        }
    }

    public class LogisticaSA : IExternalShipping
    {

        private string Nome = "LogisticaSA";
        private LogisticaSAWS.LogisticaSAClient proxy;

        public LogisticaSA()
        {
            proxy = new LogisticaSAWS.LogisticaSAClient();
        }

        public decimal GetPrecoPorPagina(int numPaginas)
        {
            return proxy.GetPriceByPage(numPaginas);
        }

        public string GetNomeServico()
        {
            return Nome;
        }
    }
}