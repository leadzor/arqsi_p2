﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Item
    {
        // Chave primaria
        public int ItemID { get; set; }

        // Chaves secundarias
        public int AutorID { get; set; }
        public int GeneroID { get; set; }

        // Propriedades
        [DisplayName("Título")]
        [Required(ErrorMessage="Insira Título")]
        public string Titulo { get; set; }
        [DisplayName("Preço")]
        [Required(ErrorMessage = "Insira Preço")]
        [DisplayFormat(DataFormatString = "{0:0.##}")]
        [DataType("Double")]
        [Range(typeof(decimal),"0", "10000000", ErrorMessage="O preço deve ser um valor numérico enntre 0.00 e 10000000")]
        public decimal Preco { get; set; }
        [DisplayName("Páginas")]
        [Required(ErrorMessage="Insira número de páginas. 0 para digital")]
        [Range(typeof(int), "0", "10000000", ErrorMessage="O numero de páginas deve ser positivo")]
        public int Paginas { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DisplayName("Data de Publicação")]
        public DateTime DataPublicacao { get; set; }

        [DisplayName("Sinopse")]
        public string Sinopse { get; set; }
        [DisplayName("Código")]
        [Required(ErrorMessage = "Insira código ISBN ou ISSN")]
        public string GUID { get; set; }
        [DisplayName("URL Imagem")]
        public string UrlImg { get; set; }

        public virtual Autor Autor { get; set; }
        public virtual Genero Genero { get; set; }
    }
}