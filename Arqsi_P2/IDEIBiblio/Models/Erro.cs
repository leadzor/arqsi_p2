﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Erro
    {
        public int ErroID { get; set; }
        public DateTime When { get; set; }
        public string Descricao { get; set; }
        public int Gravidade { get; set; }
    }
}