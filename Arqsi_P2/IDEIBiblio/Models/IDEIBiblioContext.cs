﻿using System.Data.Entity;

namespace IDEIBiblio.Models
{
    public class IDEIBiblioContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<IDEIBiblio.Models.IDEIBiblioContext>());

        public IDEIBiblioContext() : base("name=IDEIBiblioContext")
        {
        }

        public DbSet<Item> Items { get; set; }

        public DbSet<Autor> Autors { get; set; }

        public DbSet<Genero> Generoes { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Carrinho> Carrinhos { get; set; }

        public DbSet<Compra> Compras { get; set; }

        public DbSet<DetalhesCompra> DetalhesCompras { get; set; }

        public DbSet<TabelaPrecos> TabelaPrecos { get; set; }

        public DbSet<LinhaTabelaPrecos> LinhasTPrecos { get; set; }

        public DbSet<Marketer> Marketers { get; set; }

        public DbSet<Erro> Erros { get; set; }
    }
}
