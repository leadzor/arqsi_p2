﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace IDEIBiblio.Models
{
    public class Marketer
    {
        public int MarketerID { get; set; }
        public string Nome { get; set; }
        [DisplayName("Chave de acesso")]
        public string AuthKey { get; set; }

        public Marketer()
        {
            this.AuthKey = Guid.NewGuid().ToString();
        }

        public Marketer(string name)
        {
            this.Nome = name;
            this.AuthKey = Guid.NewGuid().ToString();
        }
    }
}
