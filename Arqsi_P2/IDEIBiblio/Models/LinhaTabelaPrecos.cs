﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDEIBiblio.Models
{
    public class LinhaTabelaPrecos
    {
        public int LinhaTabelaPrecosID { get; set; }
        public int TabelaPrecosID { get; set; }
        public decimal NovoPreco { get; set; }
        public int ItemID { get; set; }

        public virtual TabelaPrecos TabelaPrecos { get; set; }
        public virtual Item Item { get; set; }
    }
}
