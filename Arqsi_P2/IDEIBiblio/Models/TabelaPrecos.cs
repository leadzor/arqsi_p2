﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class TabelaPrecos
    {
        public int TabelaPrecosID { get; set; }
        public int MarketerID { get; set; }
        public DateTime DataHora { get; set; }

        public virtual List<LinhaTabelaPrecos> Linhas { get; set; }
        public virtual Marketer Marketer { get; set; }
    }
}