﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IDEIBiblio.Models
{
    public class CarrinhoCompras
    {
        IDEIBiblioContext db = new IDEIBiblioContext();

        string CarrinhoComprasID { get; set; }
        public const string Sessao = "CarrinhoID";
        
        public static CarrinhoCompras GetCarrinho(HttpContextBase context)
        {
            var carrinho = new CarrinhoCompras();
            carrinho.CarrinhoComprasID = carrinho.GetCarrinhoID(context);
            return carrinho;
        }

        public string GetCarrinhoID(HttpContextBase context)
        {
            if (context.Session[Sessao] == null)
            {
                // se o utilizador anonimo voltou
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[Sessao] = context.User.Identity.Name;
                }
                else // se novo utilizador, gerar id temporario
                {
                    Guid anonGuid = Guid.NewGuid();
                    context.Session[Sessao] = anonGuid.ToString();
                }
            }
            return context.Session[Sessao].ToString();
        }

        // converte carrinho de anonimo em utilizador
        public void AssociarCarrinho(string username)
        {
            var carrinhoCompras = db.Carrinhos.Where(c => c.AnonCarrinhoID == CarrinhoComprasID);
            foreach (Carrinho item in carrinhoCompras)
            {
                item.AnonCarrinhoID = username;
            }
            db.SaveChanges();
        }

        public void Adicionar(Item item)
        {
            var itemCarrinho = db.Carrinhos.SingleOrDefault(
                c => c.AnonCarrinhoID == CarrinhoComprasID
                && c.ItemID == item.ItemID);

            if (itemCarrinho == null)
            {
                itemCarrinho = new Carrinho
                {
                    ItemID = item.ItemID,
                    AnonCarrinhoID = CarrinhoComprasID,
                    Numero = 1,
                    DataC = DateTime.Now
                };
                db.Carrinhos.Add(itemCarrinho);
            }
            else
            {
                itemCarrinho.Numero++;
            }
            db.SaveChanges();
        }

        public int Remover(int id)
        {
            var itemCarrinho = db.Carrinhos.Single(cart => cart.AnonCarrinhoID == CarrinhoComprasID && cart.ItemID == id);

            int itemCount = 0;

            if (itemCarrinho != null)
            {
                if (itemCarrinho.Numero > 1)
                {
                    itemCarrinho.Numero--;
                    itemCount = itemCarrinho.Numero;
                }
                else
                {
                    db.Carrinhos.Remove(itemCarrinho);
                }
                db.SaveChanges();
            }
            return itemCount;
        }

        public void Esvaziar()
        {
            var cartItems = db.Carrinhos.Where(
                cart => cart.AnonCarrinhoID == CarrinhoComprasID);

            foreach (var cartItem in cartItems)
            {
                db.Carrinhos.Remove(cartItem);
            }
            db.SaveChanges();
        }

        public List<Carrinho> GetItemsCarrinho()
        {
            return db.Carrinhos.Where(
                cart => cart.AnonCarrinhoID == CarrinhoComprasID).ToList();
        }

        public int GetNumero()
        {
            // Get the count of each item in the cart and sum them up
            int? count = (from cartItems in db.Carrinhos
                          where cartItems.AnonCarrinhoID == CarrinhoComprasID
                          select (int?)cartItems.Numero).Sum();
            // Return 0 if all entries are null
            return count ?? 0;
        }
        public decimal GetTotal()
        {
            decimal? total = (from cartItems in db.Carrinhos
                              where cartItems.AnonCarrinhoID == CarrinhoComprasID
                              select (int?)cartItems.Numero * cartItems.Item.Preco).Sum();

            return total ?? decimal.Zero;
        }

        public int GetTotalPaginas()
        {
            int total = (from cartItems in db.Carrinhos
                         where cartItems.AnonCarrinhoID == CarrinhoComprasID
                         select (int)cartItems.Item.Paginas).Sum();
            return total;
        }

        public int CriarCompra(Compra compra)
        {
            decimal totalCompra = 0;

            var cartItems = GetItemsCarrinho();
            // Iterate over the items in the cart, 
            // adding the order details for each
            foreach (var item in cartItems)
            {
                var detalheCompra = new DetalhesCompra
                {
                    ItemID = item.ItemID,
                    CompraID = compra.CompraID,
                    Preco = item.Item.Preco,
                    Quantidade = item.Numero
                };
                // Set the order total of the shopping cart
                totalCompra += (item.Numero * item.Item.Preco);

                db.DetalhesCompras.Add(detalheCompra);

            }
            // Set the order's total to the orderTotal count
            compra.Total = totalCompra;
            compra.TotalPag = GetTotalPaginas();
            db.Entry(compra).State = EntityState.Modified;
            // Save the order
            db.SaveChanges();
            // Empty the shopping cart
            Esvaziar();
            // Return the OrderId as the confirmation number
            return compra.CompraID;
        }


    }
}