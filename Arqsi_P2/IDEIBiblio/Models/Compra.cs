﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIBiblio.Models
{
    public class Compra
    {
        [ScaffoldColumn(false)]
        public int CompraID { get; set; }

        [ScaffoldColumn(false)]
        public string Username { get; set; }

        [Required(ErrorMessage="Insira primeiro nome")]
        [DisplayName("Primeiro Nome")]
        [StringLength(60)]
        public string PrimeiroNome { get; set; }


        [Required(ErrorMessage = "Insira último nome")]
        [DisplayName("Último Nome")]
        [StringLength(60)]
        public string UltimoNome { get; set; }

        [Required(ErrorMessage = "Insira a morada")]
        [StringLength(160)]
        public string Morada { get; set; }

        [Required(ErrorMessage = "Insira a Localidade")]
        [StringLength(60)]
        public string Localidade { get; set; }

        [Required(ErrorMessage = "Insira código postal")]
        [StringLength(10)]
        [DisplayName("Código Postal")]
        public string CodigoPostal { get; set; }

        [Required(ErrorMessage = "Insira código postal")]
        [StringLength(30)]
        [DisplayName("País")]
        public string Pais { get; set; }

        [Required(ErrorMessage = "Insira contacto telefónico")]
        [StringLength(15)]
        [RegularExpression(@"(\+)?[0-9]+", ErrorMessage = "Número telefónico inválido")]
        public string Telefone { get; set; }

        [Required(ErrorMessage = "Insira email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email inválido")]
        public string Email { get; set; }

        [ScaffoldColumn(false)]
        public DateTime Data { get; set; }

        [ScaffoldColumn(false)]
        public decimal Total { get; set; }

        [ScaffoldColumn(false)]
        public int TotalPag { get; set; }

        public virtual List<DetalhesCompra> DetalhesCompras { get; set; }

        [ScaffoldColumn(false)]
        [NotMapped]
        public List<ShippingServiceResults> ssr { get; set; }
    }
}