﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDEIBiblio.Models
{
    public class DetalhesCompra
    {
        public int DetalhesCompraID { get; set; }
        public int CompraID { get; set; }
        public int ItemID { get; set; }
        public int Quantidade { get; set; }
        public decimal Preco { get; set; }

        public virtual Item Item { get; set; }
        public virtual Compra Compra { get; set; }
    }
}
