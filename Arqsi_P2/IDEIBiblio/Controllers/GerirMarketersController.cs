﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize(Roles = "Admins")]
    public class GerirMarketersController : Controller
    {
        private IDEIBiblioContext db = new IDEIBiblioContext();

        //
        // GET: /GerirMarketers/

        public ActionResult Index()
        {
            return View(db.Marketers.ToList());
        }

        //
        // GET: /GerirMarketers/Details/5

        public ActionResult Details(int id = 0)
        {
            Marketer marketer = db.Marketers.Find(id);
            if (marketer == null)
            {
                return HttpNotFound();
            }
            return View(marketer);
        }

        //
        // GET: /GerirMarketers/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GerirMarketers/Create

        [HttpPost]
        public ActionResult Create(FormCollection values)
        {
            Marketer marketer = new Marketer();
            if (ModelState.IsValid)
            {
                TryUpdateModel(marketer);
                db.Marketers.Add(marketer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marketer);
        }

        //
        // GET: /GerirMarketers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Marketer marketer = db.Marketers.Find(id);
            if (marketer == null)
            {
                return HttpNotFound();
            }
            return View(marketer);
        }

        //
        // POST: /GerirMarketers/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Marketer marketer = db.Marketers.Find(id);
            db.Marketers.Remove(marketer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}