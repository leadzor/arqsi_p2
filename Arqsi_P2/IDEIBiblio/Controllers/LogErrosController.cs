﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize(Roles="Admins")]
    public class LogErrosController : Controller
    {
        IDEIBiblioContext db = new IDEIBiblioContext();
        //
        // GET: /LogErros/

        public ActionResult Index()
        {
            List<Erro> erros = db.Erros.OrderByDescending(e => e.When).ToList();
            return View(erros);
        }

    }
}
