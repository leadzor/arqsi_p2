﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using IDEIBiblio.ViewModels;


namespace IDEIBiblio.Controllers
{
    public class VerTabelasPrecoController : Controller
    {

        IDEIBiblioContext db = new IDEIBiblioContext();
        //
        // GET: /VerTabelasPreco/
        
        public ActionResult Index()
        {
            List<ComparacaoPrecosViewModel> lista = new List<ComparacaoPrecosViewModel>();

            List<LinhaTabelaPrecos> precos = 
                db.TabelaPrecos.OrderByDescending(item => item.DataHora).First().Linhas;

            foreach(LinhaTabelaPrecos lt in precos)
            {
                if (db.Items.Any(a=>a.ItemID == lt.ItemID))
                {
                    lista.Add(new ComparacaoPrecosViewModel() { Item = db.Items.Find(lt.ItemID), novoPreco = lt.NovoPreco});
                }
            }
            
            return View(lista);
        }

    }
}
