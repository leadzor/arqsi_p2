﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize(Roles = "Admins,Managers")]
    public class GerirGenerosController : Controller
    {
        private IDEIBiblioContext db = new IDEIBiblioContext();

        //
        // GET: /GerirGeneros/

        public ActionResult Index()
        {
            return View(db.Generoes.ToList());
        }

        //
        // GET: /GerirGeneros/Details/5

        public ActionResult Details(int id = 0)
        {
            Genero genero = db.Generoes.Find(id);
            if (genero == null)
            {
                return HttpNotFound();
            }
            return View(genero);
        }

        //
        // GET: /GerirGeneros/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GerirGeneros/Create

        [HttpPost]
        public ActionResult Create(Genero genero)
        {
            if (ModelState.IsValid)
            {
                db.Generoes.Add(genero);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(genero);
        }

        //
        // GET: /GerirGeneros/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Genero genero = db.Generoes.Find(id);
            if (genero == null)
            {
                return HttpNotFound();
            }
            return View(genero);
        }

        //
        // POST: /GerirGeneros/Edit/5

        [HttpPost]
        public ActionResult Edit(Genero genero)
        {
            if (ModelState.IsValid)
            {
                db.Entry(genero).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(genero);
        }

        //
        // GET: /GerirGeneros/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Genero genero = db.Generoes.Find(id);
            if (genero == null)
            {
                return HttpNotFound();
            }
            return View(genero);
        }

        //
        // POST: /GerirGeneros/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Genero genero = db.Generoes.Find(id);
            db.Generoes.Remove(genero);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}