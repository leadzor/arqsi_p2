﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Filters;

namespace IDEIBiblio.Controllers
{
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "página inicial";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Sobre este website.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "contactos pessoais.";

            return View();
        }
    }
}
