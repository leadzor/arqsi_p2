﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize]
    public class CompraController : Controller
    {

        IDEIBiblioContext db = new IDEIBiblioContext();
        Compra compra = null;
        List<IExternalShipping> ShippingPartners = new List<IExternalShipping>()
        {
            new ShippingAll(),
            new LogisticaSA()
        };

        //
        // GET: /Encomendar/Dados
        
        public ActionResult Dados()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Dados(FormCollection formulario)
        {
            compra = new Compra();
            TryUpdateModel(compra);
            try
            {
                compra.Username = User.Identity.Name;
                compra.Data = DateTime.Now;
                db.Compras.Add(compra);
                db.SaveChanges();
                CarrinhoCompras carrinho = CarrinhoCompras.GetCarrinho(this.HttpContext);
                carrinho.CriarCompra(compra);
                return RedirectToAction("Envio", new { id = compra.CompraID });

            }
            catch (Exception ex)
            {
                return View(compra);
            }
        }

        public ActionResult Envio(int id)
        {
            bool valido = db.Compras.Any(o => o.CompraID == id && o.Username == User.Identity.Name);
            if (valido)
            {
                Compra notaEncomenda = db.Compras.Find(id);
                notaEncomenda.ssr = new List<ShippingServiceResults>();
                foreach (IExternalShipping service in ShippingPartners)
                {
                    notaEncomenda.ssr.Add(new ShippingServiceResults()
                    {
                        ShippingService = service.GetNomeServico(),
                        Price = Math.Round(service.GetPrecoPorPagina(notaEncomenda.TotalPag),2)
                    });
                }
                return View(notaEncomenda);
            }
            else
            {
                db.Erros.Add(new Erro() { Descricao = "Page Access Fault -  - " + this.GetType(), When = DateTime.Now, Gravidade = 2 });
                db.SaveChanges();
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Envio(FormCollection values)
        {
            
            return RedirectToAction("Concluida", new { id = values["hidden"] });
        }




        public ActionResult Concluida(int id)
        {
            bool valido = db.Compras.Any(o => o.CompraID == id && o.Username == User.Identity.Name);
            if (valido)
            {
                return View(id);
            }
            else
            {
                db.Erros.Add(new Erro() { Descricao = "Page Access Fault - " + this.GetType(), When = DateTime.Now, Gravidade = 2 });
                db.SaveChanges();
                return View("Error");
            }
        }

    }
}
