﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;
using IDEIBiblio.ViewModels;

namespace IDEIBiblio.Controllers
{
    public class CarrinhoController : Controller
    {

        IDEIBiblioContext db = new IDEIBiblioContext();
        //
        // GET: /Carrinho/

        public ActionResult Index()
        {
            var cart = CarrinhoCompras.GetCarrinho(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new CarrinhoComprasViewModel
            {
                ItemsCarrinho = cart.GetItemsCarrinho(),
                TotalCarrinho = cart.GetTotal()
            };
            // Return the view
            return View(viewModel);
        }

        // GET: /Carrinho/Adicionar/5
        public ActionResult Adicionar(int id)
        {
            var itemAdicionado = db.Items.Single(item => item.ItemID == id);

            // Add it to the shopping cart
            var carrinho = CarrinhoCompras.GetCarrinho(this.HttpContext);

            carrinho.Adicionar(itemAdicionado);

            // Go back to the main store page for more shopping
            return RedirectToAction("Index");
        }
        //
        // AJAX: /Carrinho/Remover/5
        [HttpPost]
        public ActionResult Remover(int id)
        {
            // Remove the item from the cart
            var carrinho = CarrinhoCompras.GetCarrinho(this.HttpContext);

            string nome = db.Carrinhos.Single(item => item.ItemID == id).Item.Titulo;

            // Remove from cart
            int itemCount = carrinho.Remover(id);

            // Display the confirmation message
            var results = new RemoverCarrinhoComprasViewModel
            {
                Mensagem = nome + " foi removido do carrinho.",
                TotalCarrinho = carrinho.GetTotal(),
                CountCarrinho = carrinho.GetNumero(),
                NumItems = itemCount,
                RemoverID = id
            };
            return Json(results);
        }
        //
        // GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult Sumario()
        {
            var carrinho = CarrinhoCompras.GetCarrinho(this.HttpContext);

            ViewData["CarrinhoCount"] = carrinho.GetNumero();
            return PartialView("Sumario");
        }

    }
}
