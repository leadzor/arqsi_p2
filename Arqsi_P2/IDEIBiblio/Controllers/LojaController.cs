﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    public class LojaController : Controller
    {
        IDEIBiblio.Models.IDEIBiblioContext db = new IDEIBiblio.Models.IDEIBiblioContext();
        //
        // GET: /Loja/
        public ActionResult Index()
        {
            var items = db.Items.ToList();
            return View(items);
        }

        //
        // GET: /Loja/Filtrar?genero=XXX
        public ActionResult Filtrar(string genero)
        {
            if (db.Generoes.Any(g => g.Nome == genero))
            {
                var gen = db.Generoes.SingleOrDefault(g => g.Nome == genero);
                var itemList = db.Items.Where(i => i.GeneroID == gen.GeneroID).ToList();
                ViewBag.genero = gen.Nome;
                return View(itemList);
            }
            db.Erros.Add(new Erro() { Descricao = "Page not found - " + this.GetType(), When = DateTime.Now, Gravidade = 1 });
            db.SaveChanges();
            return View("Error");
        }

        public ActionResult Detalhes(int id)
        {
            if(db.Items.Find(id) == null)
            {
                db.Erros.Add(new Erro() { Descricao = "Page not found - " + this.GetType(), When = DateTime.Now, Gravidade = 1 });
                db.SaveChanges();
                return View("Error");
            }
            Item item = db.Items.Include("Genero").SingleOrDefault(i => i.ItemID == id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        public ActionResult GenerosLateral()
        {
            var generos = db.Generoes.ToList();
            return PartialView(generos);
        }

    }
}
