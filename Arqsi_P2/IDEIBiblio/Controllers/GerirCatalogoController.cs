﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize(Roles = "Admins,Managers")]
    public class GerirCatalogoController : Controller
    {
        private IDEIBiblioContext db = new IDEIBiblioContext();

        //
        // GET: /GerirCatalogo/

        public ActionResult Index()
        {
            var items = db.Items.Include(i => i.Autor).Include(i => i.Genero);
            return View(items.ToList());
        }

        //
        // GET: /GerirCatalogo/Details/5

        public ActionResult Details(int id = 0)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        //
        // GET: /GerirCatalogo/Create

        public ActionResult Create()
        {
            ViewBag.AutorID = new SelectList(db.Autors, "AutorID", "Nome");
            ViewBag.GeneroID = new SelectList(db.Generoes, "GeneroID", "Nome");
            return View();
        }

        //
        // POST: /GerirCatalogo/Create

        [HttpPost]
        public ActionResult Create(Item item)
        {
            if (ModelState.IsValid)
            {
                db.Items.Add(item);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AutorID = new SelectList(db.Autors, "AutorID", "Nome", item.AutorID);
            ViewBag.GeneroID = new SelectList(db.Generoes, "GeneroID", "Nome", item.GeneroID);
            return View(item);
        }

        //
        // GET: /GerirCatalogo/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            ViewBag.AutorID = new SelectList(db.Autors, "AutorID", "Nome", item.AutorID);
            ViewBag.GeneroID = new SelectList(db.Generoes, "GeneroID", "Nome", item.GeneroID);
            return View(item);
        }

        //
        // POST: /GerirCatalogo/Edit/5

        [HttpPost]
        public ActionResult Edit(Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AutorID = new SelectList(db.Autors, "AutorID", "Nome", item.AutorID);
            ViewBag.GeneroID = new SelectList(db.Generoes, "GeneroID", "Nome", item.GeneroID);
            return View(item);
        }

        //
        // GET: /GerirCatalogo/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        //
        // POST: /GerirCatalogo/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}