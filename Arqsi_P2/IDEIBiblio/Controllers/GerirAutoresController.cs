﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers
{
    [Authorize(Roles = "Admins,Managers")]
    public class GerirAutoresController : Controller
    {
        private IDEIBiblioContext db = new IDEIBiblioContext();

        //
        // GET: /GerirAutores/

        public ActionResult Index()
        {
            return View(db.Autors.ToList());
        }

        //
        // GET: /GerirAutores/Details/5

        public ActionResult Details(int id = 0)
        {
            Autor autor = db.Autors.Find(id);
            if (autor == null)
            {
                return HttpNotFound();
            }
            return View(autor);
        }

        //
        // GET: /GerirAutores/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GerirAutores/Create

        [HttpPost]
        public ActionResult Create(Autor autor)
        {
            if (ModelState.IsValid)
            {
                db.Autors.Add(autor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(autor);
        }

        //
        // GET: /GerirAutores/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Autor autor = db.Autors.Find(id);
            if (autor == null)
            {
                return HttpNotFound();
            }
            return View(autor);
        }

        //
        // POST: /GerirAutores/Edit/5

        [HttpPost]
        public ActionResult Edit(Autor autor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(autor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(autor);
        }

        //
        // GET: /GerirAutores/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Autor autor = db.Autors.Find(id);
            if (autor == null)
            {
                return HttpNotFound();
            }
            return View(autor);
        }

        //
        // POST: /GerirAutores/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Autor autor = db.Autors.Find(id);
            db.Autors.Remove(autor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}