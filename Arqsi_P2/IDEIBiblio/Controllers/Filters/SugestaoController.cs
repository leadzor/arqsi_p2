﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIBiblio.Models;

namespace IDEIBiblio.Controllers.Filters
{
    public class SugestaoController : Controller
    {
        //
        // GET: /Sugestao/

        IDEIBiblioContext db = new IDEIBiblioContext();

        [ChildActionOnly]
        public PartialViewResult Sugestoes()
        {
            List<Item> sugested = new List<Item>();
            if(this.HttpContext.User.Identity.IsAuthenticated)
            {
                var list = db.DetalhesCompras.Where(d => d.Compra.Username == this.HttpContext.User.Identity.Name);
                var gender = list.Select(d => d.Item.Genero).Distinct();
                foreach(Genero g in gender)
                {
                    foreach (Item i in g.Items)
                    {
                        if (!list.Any(a => a.ItemID == i.ItemID))
                        {
                            sugested.Add(i);
                        }
                    }
                }
            }
            return PartialView(sugested);
        }
    }
}
