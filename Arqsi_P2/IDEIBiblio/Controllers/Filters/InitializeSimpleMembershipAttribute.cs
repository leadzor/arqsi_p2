﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using IDEIBiblio.Models;

namespace IDEIBiblio.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<IDEIBiblioContext>(null);

                try
                {
                    using (var context = new IDEIBiblioContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    WebSecurity.InitializeDatabaseConnection("IDEIBiblioContext", "UserProfile", "UserId", "UserName", autoCreateTables: true);

                    if (!Roles.RoleExists("Admins"))
                    {
                        Roles.CreateRole("Admins");
                    }

                    if (!Roles.RoleExists("Managers"))
                    {
                        Roles.CreateRole("Managers");
                    }

                    if (!WebSecurity.UserExists("admin"))
                    {
                        WebSecurity.CreateUserAndAccount("admin", "dhb2znj");
                    }
                    if (!Roles.GetRolesForUser("admin").Contains("Admins"))
                    {
                        Roles.AddUserToRole("admin", "Admins");
                    }

                    if (!WebSecurity.UserExists("manager"))
                    {
                        WebSecurity.CreateUserAndAccount("manager", "dhb2znj");
                    }
                    if (!Roles.GetRolesForUser("manager").Contains("Managers"))
                    {
                        Roles.AddUserToRole("manager", "Managers");
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }
}
