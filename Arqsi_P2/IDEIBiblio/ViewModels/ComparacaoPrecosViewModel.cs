﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDEIBiblio.Models;
using System.ComponentModel;


namespace IDEIBiblio.ViewModels
{
    public class ComparacaoPrecosViewModel
    {
        public Item Item { get; set; }
        [DisplayName("Novo Preço")]
        public decimal novoPreco { get; set; }
    }
}