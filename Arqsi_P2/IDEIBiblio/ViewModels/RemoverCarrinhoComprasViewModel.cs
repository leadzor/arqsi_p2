﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IDEIBiblio.ViewModels
{
    public class RemoverCarrinhoComprasViewModel
    {
        public string Mensagem { get; set; }
        public decimal TotalCarrinho { get; set; }
        public int CountCarrinho { get; set; }
        public int NumItems { get; set; }
        public int RemoverID { get; set; }
    }
}