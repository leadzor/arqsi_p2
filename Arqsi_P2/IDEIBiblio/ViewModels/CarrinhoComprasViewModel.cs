﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDEIBiblio.Models;

namespace IDEIBiblio.ViewModels
{
    public class CarrinhoComprasViewModel
    {
        public List<Carrinho> ItemsCarrinho { get; set; }
        public decimal TotalCarrinho { get; set; }
    }
}