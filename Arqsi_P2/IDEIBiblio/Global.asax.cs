﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Data.Entity;
using System.Web.Security;
using IDEIBiblio.Models;

namespace IDEIBiblio
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            // Rebuild DB
            // DONT UNCOMMENT THIS FOR GOD'S SAKE I BEG YOU DO NOT UNCOMMENT THIS
            //Database.SetInitializer(new DatabaseInitializer());
            //IDEIBiblioContext context = new IDEIBiblioContext();
            //context.Database.Initialize(true);
            //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ READ ABOVE PLEASE :(
        }
    }

    public class DatabaseInitializer : DropCreateDatabaseAlways<IDEIBiblioContext>
    {
        protected override void Seed(IDEIBiblioContext context)
        {
            Genero[] Generos = 
                {
                    new Genero() { Nome = "Aventura" },
                    new Genero() { Nome = "Policial" },
                    new Genero() { Nome = "Culinaria" },
                    new Genero() { Nome = "Cor de Rosa" },
                    new Genero() { Nome = "Técnico" },
                    new Genero() { Nome = "Arte" },
                    new Genero() { Nome = "Música" },
                    new Genero() { Nome = "Infantil" },
                    new Genero() { Nome = "Romance" },
                    new Genero() { Nome = "Poesia" }
                };

            Autor[] Autores =
                {
                    new Autor() { Nome = "Dan Brown", Biografia = "É um autor ingles", DataNascimento = DateTime.Now },
                    new Autor() { Nome = "JK Rowling", Biografia = "É uma autora inglesa", DataNascimento = DateTime.Now },
                    new Autor() { Nome = "Saramago", Biografia = "É um autor portugues.", DataNascimento = DateTime.Now },
                    new Autor() { Nome = "Luiz de Camoes", Biografia = "É um autor historico portugues", DataNascimento = DateTime.Now },
                    new Autor() { Nome = "Fernando Pessoa", Biografia = "Era um autor portugues", DataNascimento = DateTime.Now },
                    new Autor() { Nome = "Da Vinci", Biografia = "É um autor italiano", DataNascimento = DateTime.Now }
                };

            Item[] items = 
                {
                    new Item() { Sinopse = "ABCD", GUID = "1110203", Autor = Autores.ElementAt(0), DataPublicacao = DateTime.Now, Genero = Generos.ElementAt(1), Preco = 12.60M, Titulo = "Fortaleza Digital", UrlImg = "www.google.pt" },
                    new Item() { Sinopse = "ABCD", GUID = "1110304", Autor = Autores.ElementAt(1), DataPublicacao = DateTime.Now, Genero = Generos.ElementAt(0), Preco = 15.99M, Titulo = "Harry Potter and the Deadly Hollows", UrlImg = "www.google.com" },
                    new Item() { Sinopse = "ABCD", GUID = "1110303", Autor = Autores.ElementAt(2), DataPublicacao = DateTime.Now, Genero = Generos.ElementAt(5), Preco = 99.10M, Titulo = "Caim", UrlImg = "www.yahoo.com" },
                    new Item() { Sinopse = "ABCD", GUID = "9998239", Autor = Autores.ElementAt(4), DataPublicacao = DateTime.Now, Genero = Generos.ElementAt(9), Preco = 9.99M, Titulo = "Mensagem", UrlImg = "www.sapo.pt" }
                };

            foreach (Genero Genero in Generos)
            {
                context.Generoes.Add(Genero);
            }

            foreach (Autor Autor in Autores)
            {
                context.Autors.Add(Autor);
            }

            foreach (Item item in items)
            {
                context.Items.Add(item);
            }
            context.SaveChanges();

        }

    }
}