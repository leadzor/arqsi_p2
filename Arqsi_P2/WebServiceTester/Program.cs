﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServiceTester
{
    class Program
    {
        static void Main(string[] args)
        {
            ShippingAll.ShippingAllPortTypeClient proxy_shp = new ShippingAll.ShippingAllPortTypeClient();
            LogisticaSA.LogisticaSAClient proxy_lsa = new LogisticaSA.LogisticaSAClient();

            int nump = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("ShippingAll#prpag(" + nump + "): " + proxy_shp.prpag(nump));
            Console.WriteLine("LogisticaSA#GetPriceByPage(" + nump + "): " + proxy_lsa.GetPriceByPage(nump));
        
            Console.ReadLine();

        }
    }
}
