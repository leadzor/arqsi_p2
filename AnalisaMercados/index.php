﻿<?php

$WSDLURL        = "http://wvm044.dei.isep.ipp.pt/IDEIBiblioWS/CatalogService.svc?wsdl";
$AUTHKEY        = "28e72839-b5c8-43e5-861c-1e3c55925bb3";
$EMULATEDVAR    = 15;

/*
 * Gera um valor aleatorio com uma diferença de (+/-) 0 a 15% do valor original
 */
function seedPrice($original, $variation) {
    $min = 0;
    $max = $variation * 2;
    return round(($original+($original*(rand($min,$max)-$variation)/100)),2);
}

$client = new SoapClient($WSDLURL);
$client->soap_defencoding = "UTF-8";
$result = $client->__soapCall("GetCatalog", array('parameters' => null));

echo '<h1>GetCatalog</h1><pre>';
print_r($result);
echo '</pre><hr />';

$array = $result->GetCatalogResult->CatalogPayload;
$arraySend = array('CatalogPayload' => array());
foreach ($array as $item) {
    $price = $item->Price;
    $newItem = array(
        'GlobalID' => $item->GlobalID,
        'InternalID' => $item->InternalID,
        'Price' => seedPrice($price, $EMULATEDVAR));
    array_push($arraySend['CatalogPayload'], $newItem);
}
echo '<h1>Array a enviar</h1><pre>';
print_r($arraySend);
echo '</pre>';

$params = array(
    'WSAuthKey' => $AUTHKEY,
    'inputList' => $arraySend);
echo "<pre>".print_r($resultSend = $client->__soapCall('SendList', array('parameters' => $params)))."</pre>";

