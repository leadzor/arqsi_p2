<?php

/******************************************************************************
 * ShippingAll SOAP Web Service v0.1
 * Business Logic Layer
 * Pedro Leal 2013, DEI_ISEP
 * ****************************************************************************
 */

define("KGCONST", 3);
define("GPERA4PAGE", 4.5);

function pr_paginas($num){
    $weight_total_g = $num * (GPERA4PAGE / 2);
    $weight_kg = $weight_total_g / 1000;
    return $weight_kg * KGCONST;
}

function pr_peso_kg($peso) {
    return $peso * KGCONST;
}