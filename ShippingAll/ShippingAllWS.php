<?php

require_once('lib/nusoap.php');
require_once('ShippingAllBusiness.php');

// Create the server instance
$server = new soap_server;
// Initialize WSDL support
$server->configureWSDL('ShippingAll', 'urn:ShippingAll', '', 'document');
$server->soap_defencoding = 'UTF-8';

$in = array('pag' => 'xsd:int');
$out = array('price' => 'xsd:decimal');
// registar serviço
$server->register(
        "prpag", //methodname
        $in, // parâmetros de entrada
        $out, // parâmetro de saída
        'urn:ShippingAll', // namespace
        $server->wsdl->endpoint . '#' . "prpag", // soapaction
        'document', // style
        'literal', // use
        'N/A' // documentation
);

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
// Define the method as a PHP function
function prpag($pag){
    $result = pr_paginas($pag);
    return array("price" => $result);
}