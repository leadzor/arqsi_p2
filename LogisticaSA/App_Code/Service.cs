﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

public class LogisticaSAWS : ILogisticaSA
{

    private const decimal PRICEPERPAGE = 0.005M;

    public decimal GetPriceByPage(int numPages)
    {
        return numPages * PRICEPERPAGE;
    }
}
